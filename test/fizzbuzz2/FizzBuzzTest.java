/**
 *
 */
package fizzbuzz2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * @author usui.arisa
 *
 */
class FizzBuzzTest {

	/**
	 * {@link fizzbuzz2.FizzBuzz#main(java.lang.String[])} のためのテスト・メソッド。
	 */



	/**
	 * {@link fizzbuzz2.FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 */



		//引数に9を渡したところ、「Fizz」が取得できる
		@Test
		public void test1() {
			assertEquals("Fizz",FizzBuzz.checkFizzBuzz(9));
		}

		@Test
		//引数に20を渡したところ、「Buzz」が取得できる

		public void test2() {
			assertEquals("Buzz",FizzBuzz.checkFizzBuzz(20));
		}

		@Test
		//引数に45を渡したところ、「FizzBuzz」が取得できる
		public void test3() {
			assertEquals("FizzBuzz",FizzBuzz.checkFizzBuzz(45));
		}

		@Test
		//引数に44を渡したろころ、「44」を取得できる
		public void test4() {
			assertEquals("44",FizzBuzz.checkFizzBuzz(44));
		}

		@Test
		//引数に46を渡したところ、「46」を取得できる
		public void test5() {
			assertEquals("46",FizzBuzz.checkFizzBuzz(46));
		}


}
