package fizzbuzz2;


public class FizzBuzz {


	public static void main(String [] args) {
		//挙動の確認用に1～１００までの整数を引数にして「checkFizzBuzz」を呼び出す
		for(int i = 1; i <= 100 ; i++) {

			String result = (checkFizzBuzz(i));
			System.out.println(result);
		}
	}


	 public static String checkFizzBuzz(int num){
		 String FizzBuzz = "FizzBuzz";
		 String Buzz = "Buzz";
		 String Fizz = "Fizz";


		//3と5両方の倍数なら「FizzBuzz」
		 if(num % 3 == 0 && num % 5 == 0) {
			 return FizzBuzz;
		//5の倍数なら「Buzz」
		 }else if(num % 5 == 0) {
			 return Buzz;
		//引数numが3の倍数なら「Fizz」
		 }else if(num % 3 == 0) {
			 return Fizz;
		//それ以外ならそのまま数字を戻り値として返す
		 }else{
			 return  String.valueOf(num);

		 }
	 }

}

/*
 * 引数numが3の倍数なら「Fizz」
 * 5の倍数なら「Buzz」
 * 3と5両方の倍数なら「FizzBuzz」
 * それ以外ならそのまま数字を戻り値として返す
 *
 * 引数 int
 * 戻り値 String
 */

